package com.example.pcoti2013;

public class personal_object {
	private int     id;
	private String  name;
	private int  	 weight, height;
	 
	public personal_object() {
	// TODO Auto-generated constructor stub
	}
	 	 
	public personal_object(String name, int weight, int height)
	{
	     super();
	     this.name = name;
	     this.weight = weight;
	     this.height = height;
	}
	 
	public int getId()
	{
	     return id;
	}
	 
	public void setId(int id)
	{
	     this.id = id;
	}
		 
	public String getName()
	{
	     return name;
	}
	
	public void setName(String name){
	     this.name = name;
	}
	 
	public int getHeight() {
		return height;
	}
	 	 
	public int getWeight() {
		return weight;
	}
	
	public void setHeight(int height) {
		this.height = height;
	}
	
	public void setWeight(int weight) {
		this.weight = weight;
	}
}
