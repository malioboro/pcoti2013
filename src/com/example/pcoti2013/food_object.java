package com.example.pcoti2013;

public class food_object {
	private int     id,tipe;
	private String  nama;
	private double 	karboh, kalori, protein, lemak, vitA, vitB, vitC;
	 
	public food_object() {
	// TODO Auto-generated constructor stub
	}
	 	 
	public food_object(String nama, double kalori, double karboh, double protein, double lemak, double vitA, double vitB, double vitC)
	{
	     super();
	     this.nama = nama;
	     this.kalori = kalori;
	     this.karboh = karboh;
	     this.protein = protein;
	     this.lemak = lemak;
	     this.vitA = vitA;
	     this.vitB = vitB;
	     this.vitC = vitC;
	}
	 
	public int getId()
	{
	     return id;
	}
	 
	public void setId(int id)
	{
	     this.id = id;
	}
		 
	public String getName()
	{
	     return nama;
	}
	
	public void setName(String nama){
	     this.nama = nama;
	}
	
	public double getKalori() {
		return kalori;
	}
	public double getKarboh() {
		return karboh;
	}
	public double getLemak() {
		return lemak;
	}
	public double getProtein() {
		return protein;
	}
	public double getVitA() {
		return vitA;
	}
	public double getVitB() {
		return vitB;
	}
	public double getVitC() {
		return vitC;
	}
	public void setKalori(double kalori) {
		this.kalori = kalori;
	}
	public void setKarboh(double karboh) {
		this.karboh = karboh;
	}
	public void setLemak(double lemak) {
		this.lemak = lemak;
	}
	public void setProtein(double protein) {
		this.protein = protein;
	}
	public void setVitA(double vitA) {
		this.vitA = vitA;
	}
	public void setVitB(double vitB) {
		this.vitB = vitB;
	}
	public void setVitC(double vitC) {
		this.vitC = vitC;
	}
	public int getTipe() {
		return tipe;
	}
	public void setTipe(int tipe) {
		this.tipe = tipe;
	}
}
