package com.example.pcoti2013;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class inplaukcode extends Activity{
	
	private foodDBAdapter mDb;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.laukview);
		
		ActivityHelper.initialize(this);
		mDb = foodDBAdapter.getInstance(this);
		final ArrayList<food_object> list_food = new ArrayList<food_object>();
		//urutannya nama-kalori-karbohidrat-protein-lemak-vitaminA-vitaminB-vitaminC
		list_food.add(new food_object("Ikan", 178, 40.6, 2.1, 0.1, 0, 0.02, 0));
		list_food.add(new food_object("Bintang Laut", 360, 82.1, 4.7, 0.1, 0, 0, 0));
		list_food.add(new food_object("Kerang", 83, 19.1, 2.0, 0.1, 0, 0.11, 17));
		
		
		Collections.sort(list_food, new Comparator<food_object>() {
			@Override
			public int compare(food_object lhs, food_object rhs) {
				// TODO Auto-generated method stub
				return lhs.getName().compareTo(rhs.getName());
			}
		});
		
	    ListView list;
	    final String[] food_name = new String[list_food.size()];
	    for(int i=0;i<list_food.size();i++){
	    	food_name[i]=list_food.get(i).getName();
	    }
	    
	    Integer[] imageId = {
	            R.drawable.image1,
	    };	  
	 
        customlist adapter = new
        customlist(inplaukcode.this, food_name, imageId);
		list=(ListView)findViewById(R.id.listLauk);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        	
        	@Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(inplaukcode.this, food_name[+ position] +" ditambahkan", Toast.LENGTH_SHORT).show();
                mDb.createFood(list_food.get(+ position));
                Intent toTab = new Intent(inplaukcode.this,todaycode.class); 
				startActivity(toTab);
				finish();
            }
            
        });
	}
	
	@Override
	  public void onBackPressed() {
	    this.getParent().onBackPressed();   
	  }
}